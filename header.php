	<header>
		
    <div class="navbar-fixed">
     <nav>

       <div class="container">
         <div class="nav-wrapper">

           <a href="#!" class="brand-logo"><img id="logo" src="images/logo.gif" alt="logo"></a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger black-text"><i class="material-icons">menu</i></a>

            <ul class="right hide-on-med-and-down">
              <li><a class="black-text" href="#inkaras_apgyvendinimas">Apgyvendinimas</a></li>
              <li><a class="black-text" href="#inkaras_paslaugos">Ką mes siūlome</a></li>
              <li><a class="black-text" href="#inkaras_apie">Kas mes</a></li>
              <li><a class="black-text" href="#inkaras_galerija">Galerija</a></li>
              <li><a class="black-text" href="#inkaras_kainos">Kainos</a></li>
              <li><a class="black-text" href="#inkaras_kontaktai">Kontaktai</a></li>
              <li><a class="green-text text-darken-4" href="tel:+37061529603">+37061529603</a></li>
            </ul> 
          </div>
        </div>
      </div>
    </nav> 
      </div>

<ul class="sidenav" id="mobile-demo">
  <li><a class="black-text" href="#inkaras_apgyvendinimas">Apgyvendinimas</a></li>
  <li><a class="black-text" href="#inkaras_paslaugos">Ką mes siūlome</a></li>
  <li><a class="black-text" href="#inkaras_apie">Kas mes</a></li>
  <li><a class="black-text" href="#inkaras_galerija">Galerija</a></li>
  <li><a class="black-text" href="#inkaras_kainos">Kainos</a></li>
  <li><a class="black-text" href="#inkaras_kontaktai">Kontaktai</a></li>
  <li><a class="green-text text-darken-4" href="tel:+37061529603">+37061529603</a></li>
</ul>

</header>