<div id="inkaras_paslaugos" id="inkaras">
</div>
<div class="container">
  <h4><b><i>KĄ MES SIŪLOME</i></b></h4>
  <br>
  <br>
  <div class="row" id="no_bottom_margin">
    <div class="col s5">
      <h5><b><i>Maitinimas</i></b></h5>
      <p>
        • Kaimiški pusryčiai.<br>
        • Žemaičių valgių vakarienė iš molinių indų su mediniais šaukštais.<br>
        • Iškilmėms paruošiame šventinį stalą.
      </p>
    </div>
    <div class="col s7">
      <div class="carousel paslaugos">
        <a class="carousel-item" id="foto_dydis" href="#one!"><img class="resposive-img" src="images/p_maitinimas.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#two!"><img class="resposive-img" src="images/p_maitinimas_1.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#three!"><img class="resposive-img" src="images/p_maitinimas_2.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#four!"><img class="resposive-img" src="images/p_maitinimas_3.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#five!"><img class="resposive-img" src="images/p_maitinimas_4.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#six!"><img class="resposive-img" src="images/p_maitinimas_5.jpg"></a>
      </div>
    </div>
  </div>
  <div class="row" id="no_bottom_margin">
    <div class="col s7">
      <div class="carousel paslaugos">
        <a class="carousel-item" id="foto_dydis" href="#one!"><img class="resposive-img" src="images/p_sale.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#two!"><img class="resposive-img" src="images/p_sale_1.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#three!"><img class="resposive-img" src="images/p_sale.jpg"></a>
      </div>
    </div>
    <div class="col s5">
      <h5><b><i>Salė</i></b></h5>
      <p>
        Siūlome 20 vietų salę, skirtą organizuoti seminarus, pristatymus, banketus, uždarus vakarus.
      </p>
    </div>
  </div>

  <div class="row" id="no_bottom_margin">
    <div class="col s5">
      <h5><b><i>Pirtis</i></b></h5>
      <p>
        Kaimiškoje pirtyje vienu metu gali pertis iki 6 žmonių. Drąsesni išsikaitinę gali atsigauti ežere, o jei ne – po dušu.
      </p>
    </div>
    <div class="col s7">
      <div class="carousel paslaugos">
        <a class="carousel-item" id="foto_dydis" href="#one!"><img class="resposive-img" src="images/p_pirtis.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#two!"><img class="resposive-img" src="images/p_pirtis_1.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#three!"><img class="resposive-img" src="images/p_pirtis_2.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#four!"><img class="resposive-img" src="images/p_pirtis_3.jpg"></a>
      </div>
    </div>
  </div>

  <div class="row" id="no_bottom_margin">
    <div class="col s7">
      <div class="carousel paslaugos">
        <a class="carousel-item" id="foto_dydis" href="#one!"><img class="resposive-img" src="images/p_kita.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#two!"><img class="resposive-img" src="images/p_kita_1.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#three!"><img class="resposive-img" src="images/p_kita_2.jpg"></a>
        <a class="carousel-item" id="foto_dydis" href="#four!"><img class="resposive-img" src="images/p_kita_3.jpg"></a>
      </div>
    </div>
    <div class="col s5">
      <h5><b><i>Vandens ir kitos pramogos</i></b></h5>
      <p>
        • Vandens dviračiai, valtis.<br>
        • Žūklės įrankiai.<br>
        • Maudymuisi patogi pakrantė.<br>
        • Krepšinio aikštelė.
      </p>
    </div>
  </div>
</div>