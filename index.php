<!DOCTYPE html>
<html>
<head>
	<title>Pas Tėvukus</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

	<link rel="stylesheet" type="text/css" href="styles/fullcalendar.min.css">
	<link rel="stylesheet" type="text/css" href="styles/fullcalendar.print.min.css" media="print">
	<link rel="stylesheet" type="text/css" href="styles/style.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script type="text/javascript" src="scripts/jquery.min.js"></script>
	<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
	<script type="text/javascript" src="scripts/moment.min.js"></script>
	<script type="text/javascript" src="scripts/fullcalendar.min.js"></script>

</head>
<body>
	<?php include "header.php" ?>
	<?php include "first_foto_video.php"; ?>
	<br>
	<?php include "apgyvendinimas.php"; ?>
	<br>
	<?php include "paslaugos.php"; ?>
	<br>
	<?php include "apie.php"; ?>
	<br>
	<?php include "galerija.php"; ?>
	<br>
	<?php include "kainos.php"; ?>
	<br>
	<?php include "kontaktai.php"; ?>
	<br>

	<!-- <?php


	$servername = "localhost";
	$username = "coktai_vcs0225-b";
	$password = "KukliaiPerzibtiSverme";
	$dbname = "coktai_vcs0225-b";

// Create connection
	$conn = mysqli_connect($servername, $username, $password, $dbname);

// 	if ($conn) {
// 		mysqli_set_charset($conn, "utf8");
// 		// die("Connection failed: " . mysqli_connect_error());
// 	}
	?> -->

	<!-- <div class="container">
		<form class="form-style-1">
			<input type="text" name="vardas" placeholder="Vardas">
			<input type="text" name="pavarde" placeholder="Pavarde">
			<input type="text" name="numeris" placeholder="Telefonas">
			<input type="email" name="pastas" placeholder="Paštas">
			<input type="text" name="namelis" placeholder="Namelis">
			<input type="date" name="entry_date" placeholder="nuo">
			<input type="date" name="end_date" placeholder="iki">
			<button>Pateikti</button>
		</form>
	</div> -->
	<div id="inkaras_rezervacija" id="inkaras">
	</div>
	<div class="container">
		<h4><b><i>REZERVACIJA</i></b></h4>
		<br>
		<div class="row">
			<form class="col s12">
				<div class="row">
					<div class="input-field col s6">
						<i class="material-icons prefix">account_circle</i>
						<input id="icon_prefix" type="text" class="validate">
						<label for="icon_prefix">Vardas</label>
					</div>
					<div class="input-field col s6">
						<i class="material-icons prefix">account_circle</i>
						<input id="icon_prefix" type="text" class="validate">
						<label for="icon_prefix">Pavarde</label>
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">phone</i>
						<input id="icon_telephone" type="tel" class="validate">
						<label for="icon_telephone">Telefonas</label>
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">email</i>
						<input id="email" type="email" class="validate">
						<label for="email">Paštas</label>
						<!-- <span class="helper-text" data-error="wrong" data-success="right">Helper</span> -->
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">date_range</i>
						<input id="icon_date_range" type="text" class="datepicker">
						<label for="icon_date_range">Pradžia</label>
					</div>
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">date_range</i>
						<input id="icon_date_range_2" type="text" class="datepicker">
						<label for="icon_date_range_2">Pabaiga</label>
					</div>
					<div class="input-field col s6">
						<i class="material-icons prefix">home</i>
						<select class="icons">
							<option value="" disabled selected>Pasirinkite variantą:</option>
							<option value="1" data-icon="images/kletele.jpg">"Klėtelė"</option>
							<option value="2" data-icon="images/pirtele.jpg">"Pirtelė"</option>
							<option value="3" data-icon="images/namelis.jpg">"Namelis"</option>
							<option value="4" data-icon="images/pirtis.jpg">"Pirtis"</option>
						</select>
						<label>Apgyvendinimas</label>
					</div>
					<div class="input-field col s12">
						<textarea id="textarea1" class="materialize-textarea"></textarea>
						<label for="textarea1">Komentarai</label>
					</div>
				</div>
				<div class="center-left">
					<a class="waves-effect waves-light btn green darken-4" href="#">Pateikti</a>
				</div>
			</form>
		</div>
		<br>
		<br>
	</div>


<!-- <?php

if ($conn) {


	if (isset($_GET["vardas"]) && isset($_GET["pavarde"]) && isset($_GET["numeris"]) && isset($_GET["pastas"]) && isset($_GET["namelis"]) && isset($_GET["entry_date"]) && $_GET["end_date"] != "")  {


		$sql = "INSERT INTO rezervacija (id, vardas, pavarde, numeris, pastas, namelis, entry_date, end_date)
		VALUES (null, '" . $_GET["vardas"] . "', '" . $_GET["pavarde"] . "', '" . $_GET["numeris"] . "', '" . $_GET["pastas"] . "',  '" . $_GET["namelis"] . "',  '" . $_GET["entry_date"] . "',  '" . $_GET["end_date"] . "')";


		if (mysqli_query($conn, $sql)) {

			echo "New record created successfully";

		} else {
			echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	}



	$sql = "SELECT id, vardas, pavarde, numeris, pastas, namelis, entry_date, end_date FROM rezervacija";

	$result = mysqli_query($conn, $sql);


	if (mysqli_num_rows($result) > 0) {

		echo "<table border='1'>";


    // output data of each row
		while($row = mysqli_fetch_assoc($result)) {

			echo "<tr>
			<td>" . $row["id"] . "</td>
			<td>" . $row["vardas"] . "</td>
			<td>" . $row["pavarde"] . "</td>
			<td>" . $row["numeris"] . "</td>
			<td>" . $row["pastas"] . "</td>
			<td>" . $row["namelis"] . "</td>
			<td>" . $row["entry_date"] . "</td>
			<td>" . $row["end_date"] . "</td>
			</tr>";
		}

		echo "</table>";

	} else {

		echo "0 results";
	}
}


mysqli_close($conn);

?>  -->

<div class="container">
	<h4><b><i>UŽIMTUMAS</i></b></h4>
	<br>
	<div id='calendar'></div>
	<br>
</div>

<?php include "footer.php" ?>


</body>
</html>