<footer class="page-footer">
          <div class="container">
            <div>
              <div class="col l6 s12">
                <h5 class="black-text">Kaimo turizmo sodyba „Pas Tėvukus“</h5>
                <p class="grey-text text-lighten-4">Plungės g. 42, Beržoro k., Platelių sen., LT-90419 Plungės raj.</p>
            
                 <p><a class="grey-text text-lighten-3" href="tel:+37061529603">+37061529603</a>; <a class="grey-text text-lighten-3" href="tel:+37069803485">+37069803485</a></p>
              </div>
              <div>
                <h5><a class="grey-text text-lighten-3" href="mailto:info@pastevukus.lt">info@pastevukus.lt</a></h5>
                <br>
               
              </div>
            </div>
          </div>
        </footer>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

  <script type="text/javascript" src="scripts/script.js"></script>
