<div id="inkaras_apie" id="inkaras">
</div>
<div class="container">
	<h4><b><i>KAS MES</i></b></h4>
	<br>
	<p class="slogan"><i>Etnografinio stiliaus erdvūs svetingi namai Beržoro ežero pakrantėje. Tai puiki vieta Jūsų poilsiui ar dalykiniams susitikimams.</i></p>
	<div class=row id="no_bottom_margin">
		<div class="col s12">
			<img class="responsive-img" src="images/photo_main.jpg">
		</div>
	</div>


	<div class="row">
		<div class="col s7 m6 l3 offset-m3 offset-l6 right-align">
			<img class="responsive-img" src="images/zenklas_1.jpg">
			<img class="responsive-img" src="images/zenklas_2.jpg">
			<img class="responsive-img" src="images/zenklas_3.jpg">
		</div>
		<div class="col s5 m3 l3"><img class="responsive-img" src="images/gandrai.jpg"></div>
	</div>
	<h5><i>KOMANDOS <b>NARIAI</b></i></h5>
	<br>
	<div class="row">
		<div class="col s6 m3">
			<img class="responsive-img" src="https://picsum.photos/200/300?image=0">
			<p class="nariai_size">MARIJONA<br><b>STRIAUKIENĖ</b></p>
			<p>Sodybos "Pas Tėvukus" savininkė</p>
		</div>
		<div class="col s6 m3">
			<img class="responsive-img" src="https://picsum.photos/g/200/300">
			<p class="nariai_size">MARIJONA<br><b>STRIAUKIENĖ</b></p>
			<p>Sodybos "Pas Tėvukus" savininkė</p>
		</div>
		<div class="col s6 m3">
			<img class="responsive-img" src="https://picsum.photos/200/300/?random">
			<p class="nariai_size">MARIJONA<br><b>STRIAUKIENĖ</b></p>
			<p>Sodybos "Pas Tėvukus" savininkė</p>
		</div>
		<div class="col s6 m3">
			<img class="responsive-img" src="https://picsum.photos/200/300">
			<p class="nariai_size">MARIJONA<br><b>STRIAUKIENĖ</b></p>
			<p>Sodybos "Pas Tėvukus" savininkė</p>
		</div>
	</div>
	<br>
	<br>
	<h5><i><b>LAISVALAIKIS</b></i></h5>
	<br>
	<p class="slogan"><i>Sodyba yra įsikūrusi nepakartojamo grožio Žemaitijos nacionaliniame parke, kuris išsiskiria natūralumą išlaikiusia gamta, gausa savitų šio regiono tradicijų bei lankytinų objektų. Todėl poilsio mūsų sodyboje metu, kiekvienas suras sau mielą veiklą.</i></p>
	<br>
	<div class="row">
		<div class="col s12 m4">
			<p class="nariai_size"><b>LANKYTINI<br>OBJEKTAI</b></p>
			<div class="carousel carousel-slider" id="lankytini_objektai">
				<a class="carousel-item" href="#one!"><img src="images/lankytini_objektai.jpg"></a>
				<a class="carousel-item" href="#two!"><img src="images/lankytini_objektai_1.jpg"></a>
				<a class="carousel-item" href="#three!"><img src="images/lankytini_objektai_2.jpg"></a>
				<a class="carousel-item" href="#four!"><img src="images/lankytini_objektai_3.jpg"></a>
				<a class="carousel-item" href="#five!"><img src="images/lankytini_objektai_4.jpg"></a>
			</div>
			<div class="center-align">
				<a class="waves-effect waves-teal btn-flat" id="mygtukas_previous"><i class="material-icons">navigate_before</i></a>
				<a class="waves-effect waves-teal btn-flat" id="mygtukas_next"><i class="material-icons">navigate_next</i></a>
			</div>
			<br>
			<div class="center-align">
				<a class="waves-effect waves-light btn green darken-4" href="http://zemaitijosnp.lt/turizmas/lankytini-objektai/">Plačiau</a>
			</div>
		</div>
		<div class="col s12 m4">
			<p class="nariai_size"><b>PĖSČIŲJŲ IR<br>DVIRAČIŲ TAKAI</b></p>
			<div class="carousel carousel-slider" id="takai">
				<a class="carousel-item" href="#one!"><img src="images/takai.jpg"></a>
				<a class="carousel-item" href="#two!"><img src="images/takai_1.jpg"></a>
				<a class="carousel-item" href="#three!"><img src="images/takai_2.jpg"></a>
				<a class="carousel-item" href="#four!"><img src="images/takai_3.jpg"></a>
				<a class="carousel-item" href="#five!"><img src="images/takai_4.jpg"></a>
			</div>
			<div class="center-align">
				<a class="waves-effect waves-teal btn-flat" id="mygtukas_previous_2"><i class="material-icons">navigate_before</i></a>
				<a class="waves-effect waves-teal btn-flat" id="mygtukas_next_2"><i class="material-icons">navigate_next</i></a>
			</div>
			<br>
			<div class="center-align">
				<a class="waves-effect waves-light btn green darken-4" href="http://zemaitijosnp.lt/turizmas/pesciujudviraciu-takai/">Plačiau</a>
			</div>
		</div>
		<div class="col s12 m4">
			<p class="nariai_size"><b>KITA AKTYVI<br>VEIKLA</b></p>
			<div class="carousel carousel-slider" id="kita_veikla">
				<a class="carousel-item" href="#one!"><img src="images/kita_veikla.jpg"></a>
				<a class="carousel-item" href="#two!"><img src="images/kita_veikla_1.jpg"></a>
				<a class="carousel-item" href="#three!"><img src="images/kita_veikla_2.jpg"></a>
				<a class="carousel-item" href="#four!"><img src="images/kita_veikla_3.jpg"></a>
			</div>
			<div class="center-align">
				<a class="waves-effect waves-teal btn-flat" id="mygtukas_previous_3"><i class="material-icons">navigate_before</i></a>
				<a class="waves-effect waves-teal btn-flat" id="mygtukas_next_3"><i class="material-icons">navigate_next</i></a>
			</div>
			<br>
			<div class="center-align">
				<a class="waves-effect waves-light btn green darken-4" href="http://zemaitijosnp.lt/turizmas/kita-veikla/">Plačiau</a>
			</div>
		</div>
	</div>
</div>