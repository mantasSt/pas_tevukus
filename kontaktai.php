	<div id="inkaras_kontaktai" id="inkaras">
	</div>
	<div class="container">
		<h4><b><i>KONTAKTAI</i></b></h4>
		<br>
		<div class="row">
			<div class="row">
				<div class="col s12 m12 l4">
					<p class="contact_size">
						<strong><i><b>Adresas:</b></i></strong>
						<br>Plungės g. 42, Beržoro k., Platelių sen.,
						<br>LT-90419 Plungės raj.
					</p>
					<p class="contact_size">
						<strong><i><b>Telefonas:</b></i></strong>
						<br><a class="green-text text-darken-4" href="tel:+37061529603">+37061529603</a>; <a class="green-text text-darken-4" href="tel:+37069803485">+37069803485</a>
					</p>
					<p class="contact_size">
						<strong><i><b>Paštas:</b></i></strong>
						<br><a class="green-text text-darken-4" href="mailto:info@pastevukus.lt">info@pastevukus.lt</a>
					</p>
				</div>
				<div class="col s12 m12 l8 center-align">
					<p>
						<iframe class="z-depth-5" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2229.6814405787304!2d21.820231715941127!3d56.0241891806268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e512fa5c457d03%3A0x7d3cb83b01a1b717!2sPas+t%C4%97vukus!5e0!3m2!1slt!2slt!4v1550855244223&hl=lt" width="500" height="300" frameborder="0" style="border:0" allowfullscreen ></iframe>
					</p>
				</div>
			</div>
		</div>
	</div>
