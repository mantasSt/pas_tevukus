$(document).ready(function(){
	$('.slider').slider({
		indicators: false,
		height: 450,
		interval: 4000
	});
	$('.collapsible').collapsible();
	$('.sidenav').sidenav();

  $('.slider').slider({
    duration: 700,
    interval: 60000000,
    height: 300,
  });  
  $('.carousel').carousel();
  $('.materialboxed').materialbox();
  $('.carousel.carousel-slider').carousel({
    fullWidth: true,
    indicators: true,
  });
  $('#mygtukas_previous').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#lankytini_objektai').carousel('prev')
  });
  $('#mygtukas_next').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#lankytini_objektai').carousel('next')
  });
  $('#mygtukas_previous_2').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#takai').carousel('prev')
  });
  $('#mygtukas_next_2').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#takai').carousel('next')
  });
  $('#mygtukas_previous_3').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#kita_veikla').carousel('prev')
  });
  $('#mygtukas_next_3').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('#kita_veikla').carousel('next')
  });
  $('.tabs').tabs();
  $('select').formSelect();
  $('.datepicker').datepicker();




  $('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
    	location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
    	&& 
    	location.hostname == this.hostname
    	) {
      // Figure out element to scroll to
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
        	scrollTop: target.offset().top - 64
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
          	return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }

  });


// kalendorius

$('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      defaultDate: '2019-01-12',
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectHelper: true,
      select: function(start, end) {
        var title = prompt('Event Title:');
        var eventData;
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
          $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');
      },
      editable: true,
      eventLimit: true, // allow "more" link when too many events
      events: [
        {
          title: 'All Day Event',
          start: '2019-01-01'
        },
        {
          title: 'Long Event',
          start: '2019-01-07',
          end: '2019-01-10'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-09T16:00:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-16T16:00:00'
        },
        {
          title: 'Conference',
          start: '2019-01-11',
          end: '2019-01-13'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T10:30:00',
          end: '2019-01-12T12:30:00'
        },
        {
          title: 'Lunch',
          start: '2019-01-12T12:00:00'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T14:30:00'
        },
        {
          title: 'Happy Hour',
          start: '2019-01-12T17:30:00'
        },
        {
          title: 'Dinner',
          start: '2019-01-12T20:00:00'
        },
        {
          title: 'Birthday Party',
          start: '2019-01-13T07:00:00'
        },
        {
          title: 'Click for Google',
          url: 'http://google.com/',
          start: '2019-01-28'
        }
      ]
    });

// kalendoriaus galas



});





