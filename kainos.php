 <div id="inkaras_kainos" id="inkaras">
 </div>
 <div class="container">
  <h4><b><i>KAINOS</i></b></h4>
  <br>
  <table class="highlight centered">
    <thead>
      <tr>
        <th class="green darken-4">Namas</th>
        <th class="green darken-4">Miegamųjų kambarių <br> sk.</th>
        <th class="green darken-4">Vietų sk.</th>
        <th class="green darken-4">Kaina parai</th>
        <th class="red-text green darken-4">03-01 – 05-31*<br>09-30 – 10-31*</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>"Pirtelė"</td>
        <td>2</td>
        <td>4</td>
        <td>100 Eur</td>
        <td class="red-text">80 Eur</td>
      </tr>
      <tr>
       <td>"Klėtelė"</td>
       <td>3</td>
       <td>6</td>
       <td>120 Eur</td>
       <td class="red-text">100 Eur</td>
     </tr>
     <tr>
       <td>"Namelis"**</td>
       <td>3</td>
       <td>6</td>
       <td>90 Eur</td>
       <td class="red-text">70 Eur</td>
     </tr>
     <tr>
       <td>"Pirtis"</td>
       <td>1</td>
       <td>2</td>
       <td>75 Eur</td>
       <td class="red-text">60 Eur</td>
     </tr>
   </tbody>
 </table>

 <div>* Nuolaida taikoma užsakant nakvynę ne mažiau kaip dvi paras.</div>
 <div><b>** Papildoma vieta</b> – 15 Eur</div>
 <br>

 <ul class="collapsible">
  <li>
    <div class="collapsible-header"><i class="material-icons">restaurant</i>Maitinimas</div>
    <div class="collapsible-body">
      <span>
        <ul>
          <li>Kaimiški pusryčiai 6 Eur – 1 asmeniui</li>
          <li>Žemaitiška vakarienė 20 Eur – 1 asmeniui</li>
          <li>Šventinio stalo kaina sutartinė.</li>
        </ul>
      </span>
    </div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">sentiment_very_satisfied</i>Pramogos</div>
    <div class="collapsible-body">
      <span>
        <ul>
          <li><b>Pirtis</b> – 30 Eur/2 val.</li>
          <li><b>Dviračiai</b> – 2 Eur – 1val./6 Eur dienai.</li>
          <li><b>Žūklės įrankiai</b> – 1,5 Eur/1 val.</li>
        </ul>
      </span>
    </div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">home</i>Visos sodybos nuoma</div>
    <div class="collapsible-body"><span><b>Visos sodybos kaina</b> – nuo 380 Eur parai.</span></div>
  </li>
  <li>
    <div class="collapsible-header"><i class="material-icons">priority_high</i>Pastabos</div>
    <div class="collapsible-body">
      <span>
        <ul>
          <li>Sodybos kaina negalioja švenčių dienomis ir pokyliams. Šiems užsakymams kainos derinamos individualiai.</li>
          <li><b>Vieta sodyboje „Pas tėvukus“ rezervuojama sumokėjus 30% avansą ir pasirašius užsakymo sutartį.</b></li>
        </ul>
      </span>
    </div>
  </li>
</ul>

</div>






