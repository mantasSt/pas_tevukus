<div id="inkaras_galerija" id="inkaras">
</div>
<div class="container row">
	<h4><b><i>GALERIJA</i></b></h4>
	<br>
	<div class="row">
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Pirtelė" src="images/galerija.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Klėtelė" src="images/galerija_3.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Beržoro ežeras" src="images/galerija_4.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Valtys ir vandens dviračiai" src="images/galerija_5.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Tiltas" src="images/galerija_6.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Valtis ežere" src="images/galerija_7.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Vandens dviratis" src="images/galerija_9.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Terasa" src="images/galerija_10.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Vaikai" src="images/galerija_11.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Pavėsinė" src="images/galerija_12.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Ežeras" src="images/galerija_13.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Klėtelė-Pirtelė" src="images/galerija_14.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Poilsis" src="images/galerija_15.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Poilsis" src="images/galerija_16.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Sodyba" src="images/galerija_17.jpg">
		</div>
		<div class="col s12 m6 l4">
			<img class="responsive-img card materialboxed" data-caption="Poilsis" src="images/galerija_18.jpg">
		</div>
	</div>
</div>