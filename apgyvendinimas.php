<div id="inkaras_apgyvendinimas" id="inkaras">
</div>
<div class="container">
	<h4><b><i>APGYVENDINIMAS</i></b></h4>
	<div class="row">
		<div class="col s12 m6">
			<div class="card">
				<div class="card-image waves-effect waves-block waves-light">
					<div class="slider">
						<ul class="slides">
							<li>
								<img src="images/kletele.jpg"">
							</li>
							<li>
								<img src="images/kletele_1.jpg"">
							</li>
							<li>
								<img src="images/kletele_2.jpg"">
							</li>
							<li>
								<img src="images/kletele_3.jpg"">
							</li>
							<li>
								<img src="images/kletele_4.jpg"">
							</li>
						</ul>
					</div>
				</div>
				<div class="card-content">
					<span class="card-title activator grey-text text-darken-4"><b><i>"KLĖTELĖ"</i></b><i class="material-icons right">more_vert</i></span>
					<p class="center-align">
						<a class="waves-effect waves-light btn green darken-4" href="#inkaras_rezervacija">Rezervacija</a>
					</p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">"Klėtelė"<i class="material-icons right">close</i></span>
					<p>Namas, kuriame yra 3 dviviečiai erdvūs kambariai su atskirais dušo ir wc kambariais; bendra virtuvė ir svetainė su židiniu ir vaizdu į ežerą; yra palydovinė TV, malonus poilsis lauko terasoje ant ežero kranto.</p>
					<p>Šiame name vienu metu gali ilsėtis 6 žmonės: puikus poilsis šeimai, draugų būriui ar atskiroms poroms.</p>
				</div>
			</div>
		</div>
		<div class="col s12 m6">
			<div class="card">
				<div class="card-image waves-effect waves-block waves-light">
					<div class="slider">
						<ul class="slides">
							<li>
								<img src="images/pirtele.jpg"">
							</li>
							<li>
								<img src="images/pirtele_1.jpg"">
							</li>
							<li>
								<img src="images/pirtele_2.jpg"">
							</li>
							<li>
								<img src="images/pirtele_3.jpg"">
							</li>
						</ul>
					</div>
				</div>
				<div class="card-content">
					<span class="card-title activator grey-text text-darken-4"><b><i>"PIRTELĖ"</i></b><i class="material-icons right">more_vert</i></span>
					<p class="center-align">
						<a class="waves-effect waves-light btn green darken-4" href="#inkaras_rezervacija">Rezervacija</a>
					</p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">"Pirtelė"<i class="material-icons right">close</i></span>
					<p>Namas skirtas 4 asmenų poilsiui. Čia rasite du dviviečius kambarius su patogumais antrame aukšte, o pirmajame – svetainę su židiniu, palydovine TV ir virtuvę. Bet kokiu oru malonu pasėdėti lauko terasoje ir pasidžiaugti ežero vaizdu arba pasikaitinti name įrengtoje pirtelėje.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12 m6">
			<div class="card">
				<div class="card-image waves-effect waves-block waves-light">
					<div class="slider">
						<ul class="slides">
							<li>
								<img src="images/namelis.jpg"">
							</li>
							<li>
								<img src="images/namelis_1.jpg"">
							</li>
							<li>
								<img src="images/namelis_2.jpg"">
							</li>
							<li>
								<img src="images/namelis_3.jpg"">
							</li>
						</ul>
					</div>
				</div>
				<div class="card-content">
					<span class="card-title activator grey-text text-darken-4"><b><i>"NAMELIS"</i></b><i class="material-icons right">more_vert</i></span>
					<p class="center-align">
						<a class="waves-effect waves-light btn green darken-4" href="#inkaras_rezervacija">Rezervacija</a>
					</p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">"Namelis"<i class="material-icons right">close</i></span>
					<p>Tai 3 miegamųjų kaimiško stiliaus namas. Yra visi patogumai: wc, dušai, mini virtuvė, svetainė su židiniu ir palydovine TV. Vėsiu oru jaukumo suteiks užkurta senovinė krosnis, o rąstinės sienos ir praeities atributai leis pajusti tikrojo kaimo dvasią.</p>
				</div>
			</div>
		</div>
		<div class="col s12 m6">
			<div class="card">
				<div class="card-image waves-effect waves-block waves-light">
					<div class="slider">
						<ul class="slides">
							<li>
								<img src="images/pirtis.jpg"">
							</li>
							<li>
								<img src="images/pirtis_1.jpg"">
							</li>
							<li>
								<img src="images/pirtis_2.jpg"">
							</li>
							<li>
								<img src="images/pirtis_3.jpg"">
							</li>
						</ul>
					</div>
				</div>
				<div class="card-content">
					<span class="card-title activator grey-text text-darken-4"><b><i>"PIRTIS"</i></b><i class="material-icons right">more_vert</i></span>
					<p class="center-align">
						<a class="waves-effect waves-light btn green darken-4" href="#inkaras_rezervacija">Rezervacija</a>
					</p>
				</div>
				<div class="card-reveal">
					<span class="card-title grey-text text-darken-4">"Pirtis"<i class="material-icons right">close</i></span>
					<p>Kaimiški apartamentai dviem. Rąstinis namas, kuriame yra miegamasis, wc, dušai, palydovinė TV, mini virtuvė, pirtis. Pro didžiulį panoraminį langą atsiveria nepakartojamas ežero vaizdas.</p>
				</div>
			</div>
		</div>
	</div>
</div>